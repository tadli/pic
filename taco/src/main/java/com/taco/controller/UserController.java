package com.taco.controller;

import org.springframework.web.bind.annotation.RestController;

import com.taco.mapper.UserMapper;

import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
class UserController {

    @Resource
    private UserMapper userMapper;

    @GetMapping("/hello")
    public String test() {
        System.out.println(userMapper.getUserById(1).toString());
        return "1";
    }
}