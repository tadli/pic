package com.taco.pojo.user;

import java.io.Serializable;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull(message = "Username cannot be null")
    @Size(min = 2, max = 10, message = "Username must be between 2 and 10 characters long")
    String name;

    @NotNull(message = "Password cannot be null")
    @Size(min = 6, max = 100, message = "Password must be between 6 and 100 characters long")
    private String password;

    @NotNull(message = "Email cannot be null")
    @Email(message = "Invalid email address")
    private String email;

    @NotNull(message = "Email cannot be null")
    private boolean gender;
}